import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from '../components/guest/auth.service';

@Injectable()
export class GuestGuard implements CanActivate {

  constructor(private _auth: AuthService, private _router: Router) {}

  canActivate() {
    if (!this._auth.loggedIn()) {
      return true;
    } else {
      this._router.navigate(['/']);
      return false;
    }
  }
}