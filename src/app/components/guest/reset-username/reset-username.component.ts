import * as EmailValidator from 'email-validator';
import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ResetUsernameService } from './reset-username.service';

@Component({
  selector: 'app-reset-username',
  templateUrl: './reset-username.component.html',
  styleUrls: ['./reset-username.component.scss']
})
export class ResetUsernameComponent implements OnInit {
  loading;
  success;
  error;
  public form: FormGroup;

      constructor(
        private _fb: FormBuilder,
        private _resetUsernameService: ResetUsernameService
      ) {}

       public ngOnInit() {
        this.form = this._fb.group({
           email: ['', [Validators.required, Validators.minLength(5), EmailValidator.validate]]
         });
       }

       onSubmit() {
        this.loading = true;
        this.success = false;
        this.error = false;
         this._resetUsernameService.resetUsername(this.form.value['email'])
          .subscribe(
            data => {
              this.loading = false;
              this.success = true;
            },
            error => {
              this.loading = false;
              this.error = true;
            });
       }

}
