import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  status;
  loading: boolean;
  error: boolean;
  public form: FormGroup;

    constructor(
      private _fb: FormBuilder,
      private _loginService: LoginService,
      private _router: Router
    ) {}

     public ngOnInit() {
      this.form = this._fb.group({
        username: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-Z]+$/)]
        ],
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern(/^(?=.*\d)(?=.*[a-zA-Z])(?!.*\s).*$/)]]
       });
     }

     onSubmit() {
      this.loading = true;
      this.error = false;
      this.status = false;
       this._loginService.login(this.form.value)
        .subscribe(
          data => {
            localStorage.setItem('token', data['_body']);
            this._router.navigate(['/courses']);
          },
          error => {
            this.status = error['_body'];
            this.error = true;
            this.loading = false;
          });
     }

}
