import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AuthService } from '../auth.service';

@Injectable()
export class LoginService extends AuthService {

    constructor(
        private _http: Http
    ) {
        super(_http);
    }

    login(data) {
        return this.http.post(`${this.URL}users/authenticate`, data, this.options);
    }
}