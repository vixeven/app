import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as EmailValidator from 'email-validator';

import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  success: boolean;
  loading = false;
  status;
  public form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _registerService: RegisterService
  ) {}

  ngOnInit() {
    this.form = this._fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*\d)(?=.*[a-zA-Z])(?!.*\s).*$/)]],
      confirmPassword: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern(/^[a-zA-Z]+$/)
      ]],
      email: ['', [Validators.required, Validators.email]]
    }, {
      validator: this._registerService.matchPassword
    });
  }

  public onSubmit() {
    this.loading = true;
    this.success = false;
    this.status = '';
    this._registerService.register(this.form.value)
      .subscribe(
        data => {
          this.success = true;
          this.loading = false;
          this.form.reset();
        },
        err => {
          this.status = err['_body'];
          this.loading = false;
        });
  }
}
