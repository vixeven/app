import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import {AbstractControl} from '@angular/forms';

import { AuthService } from '../auth.service';

@Injectable()
export class RegisterService extends AuthService {

    constructor(
        private _http: Http
    ) {
        super(_http);
    }

    register(data) {
        return this.http.post(`${this.URL}users/register`, data, this.options);
    }

    matchPassword(AC: AbstractControl) {
        const password = AC.get('password').value;
        const confirmPassword = AC.get('confirmPassword').value;
         if (password !== confirmPassword) {
             AC.get('confirmPassword').setErrors( {MatchPassword: true} );
         } else {
             return null;
         }
     }

}